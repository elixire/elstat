all: release

clean:
	rm -f release.tar release.tar.xz

release:
	shards build --progress --release --static
  # sync up the files here with .gitlab-ci.yml
	tar cvf ./release.tar ./bin/elstat ./config.example.ini ./scripts
	xz ./release.tar
