# elstat

Status page and health check server.

## Installing

It is recommended to use the CI builds of elstat for production deployments.

## Building for Development

Requirements:

- [Crystal] 0.36.0

[crystal]: https://crystal-lang.org/

```
git clone https://gitlab.com/elixire/elstat
cd elstat

# edit config.ini as you wish
cp config.example.ini config.ini

shards install

shards build && ./bin/elstat
```

## Building for Production

You can make a self-contained tarball of elstat (containing a statically linked
binary compiled in release mode) like so:

```
make clean
make release
```

## Note about running the elstat binary

The binary MUST have the `config.ini` file in the same working directory the
binary is running. With that premise fulfilled, just run the binary.

## Frontend

elstat is a server that provides an API only, no HTML. The frontend bits are
decoupled from the backend.

Available frontends:

- [shenlong](https://gitlab.com/elixire/shenlong)

Refer to each frontend's `README` file for installation instructions.

## Running tests

```
crystal spec
```

## Operational

### Cleaning up the database

It is important to clean up the elstat database from time to time. There is a
helper script to aid this process.

The script will vacuum the database to reclaim the space. It is important to
review the [SQLite documentation on it](https://sqlite.org/lang_vacuum.html).

It is recommended to make regular backups of the elstat database for future
analysis of uptime data.

`./scripts/wipe_data.sh ./config.ini ./elstat.db '4 months ago'`

### Removing a service

After removing the service from the configuration file, you should also remove
the related table in the `elstat.db` file so it doesn't consume unecessary disk space.

```
sqlite3 ./elstat.db 'DROP TABLE my_old_service;'

# please check sqlite3 documentation around VACUUM
sqlite3 ./elstat.db 'VACUUM;'
```
