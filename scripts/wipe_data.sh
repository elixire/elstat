#!/bin/sh
# wipe_data.sh - wipe old data off elstat.db
#
# REQUIRES:
#  GNU date
#  sqlite3
#  gawk, GNU awk

set -o errexit
set -o nounset

config_file=$1
database=$2
time_marker=$3

usage() {
    echo "usage: $0 config_file database time_marker"
    echo " time_marker can be any valid value for GNU date's -d field"
    echo " example: $0 ./config.ini ./elstat.db '6 months ago'"
    echo "  all data older than 6 months ago will be deleted"
    exit 1
}

if [ "$config_file" = "" ]; then
    usage
fi

if [ ! -f "$config_file" ]; then
    echo "WARN: given config file ($config_file) is not a file"
    usage
fi

if [ "$database" = "" ]; then
    usage
fi

if [ "$time_marker" = "" ]; then
    usage
fi

timestamp_marker=$(date -d "$time_marker" +%s%3N)
human_time_marker=$(date -d "$time_marker")

echo "all data in $database created before $human_time_marker will be deleted"
echo "confirm? [y/n]"
read confirmation

if [ "$confirmation" != "y" ]; then
    echo "not confirmed"
    exit 1
fi

program='match($0, /^\[service:([a-z]+)\]$/, groups) { print groups[1] }'

old_ifs="$IFS"
newline='
'
IFS="$newline"

for service in $(gawk "$program" "$config_file"); do
    echo "wiping data off service $service"

    rows_before=$(sqlite3 "$database" "SELECT count(*) FROM $service")
    echo "before: $rows_before rows"

    sql="DELETE FROM $service WHERE timestamp < $timestamp_marker"
    echo "exec sql: $sql"
    sqlite3 "$database" "$sql"

    rows_after=$(sqlite3 "$database" "SELECT count(*) FROM $service")
    echo "after: $rows_after rows"

    echo "ok"
done

IFS=$old_ifs

echo "vacuuming"
sqlite3 "$database" "VACUUM;"
echo "ok"

echo "all done"
