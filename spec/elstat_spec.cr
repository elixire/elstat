require "json"
require "./spec_helper"

describe "Elstat" do
  it "renders /" do
    get "/"
    response.status_code.should eq 200
  end

  # TODO: we need to somehow add some test services
  # to check anything lol.

  it "can request quick status" do
    get "/api/quick"
    response.status_code.should eq 200
    rjson = JSON.parse response.body
    rjson["status"]?.nil?.should eq false

    rjson["uptime"]?.nil?.should eq false
    rjson["week_uptime"]?.nil?.should eq false
    rjson["month_uptime"]?.nil?.should eq false
  end

  it "can request full status" do
    get "/api/status"
    response.status_code.should eq 200

    # TODO: graph checking??
    rjson = JSON.parse response.body
    rjson["graph"]?.nil?.should eq false
  end
end
