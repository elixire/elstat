require "./adapter"

class PingAdapter < Adapter
  @@PING_LATENCY_RGX = /time\=(\d+(\.\d+)?) ms/

  def self.query(ctx, serv_cfg)
    stdout = IO::Memory.new

    proc = Process.new(
      "ping",
      args: ["-c", "1", serv_cfg["ping_addr"]],
      output: stdout,
      error: stdout)

    status = proc.wait
    buffer = stdout.to_s

    alive = status.success?

    # extract latency data by attempting to parse it as a number instead of
    # only relying on regex
    latency = @@PING_LATENCY_RGX.match(buffer)
    if latency
      lat_i64 = latency[1].to_i64?
      lat_f64 = latency[1].to_f64?

      if lat_i64.nil? && lat_f64.nil?
        latency = 0.to_i64
      elsif lat_i64.nil? && !lat_f64.nil?
        latency = Math.max(lat_f64, 1.to_f64)
      elsif !lat_i64.nil?
        latency = lat_i64
      else
        latency = 0.to_i64
      end
    else
      latency = 0.to_i64
    end

    # always cast to i64
    if latency.is_a?(Float64)
      latency = latency.to_i64
    end

    unless alive
      raise AdapterError.new("packet lost")
    end

    return AdapterResult.new(alive, latency)
  end
end
