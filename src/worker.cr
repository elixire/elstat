require "./manager"
require "./adapters"
require "./context"

alias WorkerMessage = Tuple(String, Tuple(Symbol, AdapterResult | NotifyMessage))

enum NotifyType
  Created
  Crash
end

class NotifyMessage
  property notification_type : NotifyType
  property message : String

  def initialize(@notification_type, @message)
  end
end

class ServiceWorker
  property id : UUID
  property name : String
  property service : Service
  property channel : Channel(WorkerMessage)
  property ctx : Context
  property manager : Manager

  def initialize(@name, @service, @channel, @ctx, @manager)
    @id = UUID.random
  end

  def send_adapter_result(status : Bool, latency : Int64, message : String)
    @channel.send({@name, {:result, AdapterResult.new(status, latency, message)}})
  end

  def send_adapter_result(result : AdapterResult)
    @channel.send({@name, {:result, result}})
  end

  def send_notify_result(notify_type : NotifyType, message : String)
    res = NotifyMessage.new(notify_type, message)
    @channel.send({@name, {:notify, res}})
  end

  def sleep_jitter
    poll_delay = @service["poll"].to_u32
    jitter = Random.rand(0.1..10.0)
    sleep (poll_delay + jitter).seconds
  end

  def run
    self.send_notify_result(NotifyType::Created, @id.to_s)

    sleep_sec = Random.rand(1.0..4.0).round 2
    @ctx.log.info("worker start #{@name} in #{sleep_sec}s")
    sleep sleep_sec.seconds
    @ctx.log.info("worker start #{@name}!")

    adapter_name = service["adapter"]
    adapter = ADAPTERS[adapter_name]

    loop do
      begin
        worker_id = manager.@workers[@name]?
        if worker_id != nil && worker_id != (@id.to_s)
          @ctx.log.warn("another worker (#{worker_id}) has superseded this worker (#{@id})")
          return
        end

        result = adapter.query(@ctx, service)
        self.send_adapter_result(result)
        self.sleep_jitter
      rescue ex : AdapterError
        @ctx.log.warn("adapter err '#{name}': '#{ex}'")
        self.send_adapter_result(false, 0, ex.message || "error not found")
        self.sleep_jitter
      rescue ex : Exception
        @ctx.log.error("Worker #{name} crashed. #{ex.inspect_with_backtrace}")
        self.send_notify_result(NotifyType::Crash, "worker crash. error='#{ex}'")
        break
      end
    end
  end
end
