require "db"

require "./api/incidents"
require "./manager"

class DatabaseManager
  property db : DB::Database

  def initialize(@db : DB::Database)
  end

  def ensure_incident_tables
    @db.exec "
      CREATE TABLE IF NOT EXISTS incidents (
          id text PRIMARY KEY,
          incident_type text,
          title text,
          content text,
          ongoing bool,
          start_timestamp bigint,
          end_timestamp bigint
      );"

    @db.exec "CREATE TABLE IF NOT EXISTS incident_stages (
          parent_id text REFERENCES incidents (id) NOT NULL,
          timestamp bigint,
          title text,
          content text,
          PRIMARY KEY (parent_id, timestamp)
      );"
  end

  def ensure_service_table(service_name : String)
    @db.exec "
      create table if not exists #{service_name} (
        timestamp bigint,
        status bool,
        latency bigint
      )
    "
  end

  def fetch_database_graphs(service_name : String) : Tuple(Array(GraphElement), Time::Span)
    t1 = Time.monotonic
    rows = @db.query_all(
      "select timestamp, latency from #{service_name}
      order by timestamp desc
      limit 50", as: {Int64, Int64})
    t2 = Time.monotonic
    elapsed = t2 - t1

    return rows, elapsed
  end

  # Fetch a single incident.
  def fetch_incident(incident_id : UUID) : Incident?
    incident = @db.query_one?(
      "select id, incident_type, title, content, ongoing, start_timestamp, end_timestamp
      from incidents
      where id = ?", incident_id.to_s, as: {String, String, String, String, Bool, Int64, Int64?})

    if incident.nil?
      return nil
    end

    incident = Incident.new(*incident)
    incident.stages = fetch_stages(incident_id)

    return incident
  end

  def fetch_stages(incident_id : UUID) : Array(IncidentStage)
    res = [] of IncidentStage

    rows = @db.query_all(
      "select timestamp, title, content
      from incident_stages
      where parent_id = ?
      order by timestamp asc", incident_id.to_s, as: {Int64, String, String})

    rows.each do |row|
      res.push(IncidentStage.new(*row))
    end

    return res
  end
end
